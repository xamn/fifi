# FiFi the Finance Filter Stock Tool

## Why?
With FiFi you can filter stocks from any/all exchanges!
| WARNING: Due to the powerful filter option, let only people you trust use the program. |
| --- |


## Install
1. Run `pip3 install yfinance`..
2. Clone this repo.
3. Run `python3 fifi.py` inside the directory.

## Options

### --stocks
Select the stocks you want to consider.
|What                               | Option                |
|-----------------------------------|-----------------------|
|Apple and Microsoft                 | `--stocks AAPL,MSFT`   |
|Stocks from the Stockholm exchange  | `--stocks exchange:ST` |
|Kone                                | `--stocks KNEBV.HE`    |


### --limit
Limit the input stock amount.  
Useful when you want a smaller sample size and quicker downloads.

### --no-new
Don't download new stock data for stocks not in the database.  
Usefule when exchange lists contain invalid stocks.  
For example:  
First download all stocks `--stocks exchange:HE --latest`.  
After this always add the flag `--stocks exchange:HE --latest --no-new`.  

### --date
Select the dates you want to consider
|What             |Option          |
|-----------------|----------------|
|today            |`--date today`  |
|2019             |`--date 19`     |
|February 2021    | `--date 21/2`  | 
|1st January 2021 | `--date 21/1/1`|

### --latest
Select the latest availabe date.

### --dload
Force download latest data.

### --fields
Select what fields to display.
(to get a list of availabe fields for let's say Apple you would run `--stocks AAPL ---latest`.)
|What                        |Option                        |
|----------------------------|------------------------------|
|Name of firm                |`--fields nameLong`           |
|Financial currency          |`--fields currency`           |
|EBITDA and free cashflow    |`--fields ebitda,freeCashflow`|

### --filter
Filter out unwanted stocks.  
Fields are referenced with the symbol `%`.
This is a simple python expression so code injection is possible!
|What                        |Option|
|----------------------------|------|
|P/E value between 10 and 25 |`--filter "%trailingPE >= 10 and %trailingPE <= 25"`
|Technology oriented firms   |`--filter "%sector == 'Technology'"`

### --sort
Sort output with the given field.
|What                                        |Option                                 |
|--------------------------------------------|---------------------------------------|
|Sort and display stocks sorted by P/E ratio |`--fields trailingPE --sort trailingPE`|

### --group
Display all the different values available in the stock selection.
|What                              |Optio                    |
|----------------------------------|-------------------------|
|Display all the different sectors |`--fields sector --group`|

### --verbose
More output!

## Examples

### Apple P/E
```bash
samuel@skkali0:~/code/fifi$ python3 fifi.py --stocks AAPL --latest --fields forwardPE
STOCK           DATE              FIELD                          VALUE
AAPL            21/08/19 14:44:33 forwardPE                      27.35701
```

### Sectors in the Helsinki exchange
(Notice the `--no-new` flag.)
```bash
samuel@skkali0:~/code/fifi$ python3 fifi.py --stocks exchange:HE --latest --fields sector --group --no-new
Utilities
Communication Services
Energy
Consumer Defensive
Basic Materials
Technology
Healthcare
Real Estate
Consumer Cyclical
Industrials
```

### Single stock
```bash
samuel@skkali0:~/code/fifi$ python3 fifi.py --stocks exchange:US --limit 1 --latest
STOCK           DATE              FIELD                          VALUE
POSH            21/08/20 17:20:12 zip                            94065
POSH            21/08/20 17:20:12 sector                         Consumer Cyclical
POSH            21/08/20 17:20:12 fullTimeEmployees              558
POSH            21/08/20 17:20:12 longBusinessSummary            Poshmark, Inc. operates as a social marketplace for new and secondhand style products in the United States, Canada, and Australia. The company offers apparel, footwear, home, beauty, and pet products, as well as accessories. As of December 31, 2020, it had 6.5 million active buyers. The company was formerly known as GoshPosh, Inc. and changed its name to Poshmark, Inc. in 2011. Poshmark, Inc. was incorporated in 2011 and is headquartered in Redwood City, California.
POSH            21/08/20 17:20:12 city                           Redwood City
POSH            21/08/20 17:20:12 phone                          650 262 4771
POSH            21/08/20 17:20:12 state                          CA
POSH            21/08/20 17:20:12 country                        United States
POSH            21/08/20 17:20:12 companyOfficers                []
POSH            21/08/20 17:20:12 website                        http://poshmark.com
POSH            21/08/20 17:20:12 maxAge                         1
POSH            21/08/20 17:20:12 address1                       203 Redwood Shores Parkway
POSH            21/08/20 17:20:12 industry                       Internet Retail
POSH            21/08/20 17:20:12 address2                       8th Floor
POSH            21/08/20 17:20:12 ebitdaMargins                  -0.02396
POSH            21/08/20 17:20:12 profitMargins                  -0.23541
POSH            21/08/20 17:20:12 grossMargins                   0.83823997
POSH            21/08/20 17:20:12 operatingCashflow              76023000
POSH            21/08/20 17:20:12 revenueGrowth                  0.223
POSH            21/08/20 17:20:12 operatingMargins               -0.03444
POSH            21/08/20 17:20:12 ebitda                         -7207000
POSH            21/08/20 17:20:12 targetLowPrice                 38
POSH            21/08/20 17:20:12 recommendationKey              buy
POSH            21/08/20 17:20:12 grossProfits                   218570000
POSH            21/08/20 17:20:12 targetMedianPrice              45.5
POSH            21/08/20 17:20:12 currentPrice                   26.43
POSH            21/08/20 17:20:12 currentRatio                   3.289
POSH            21/08/20 17:20:12 numberOfAnalystOpinions        8
POSH            21/08/20 17:20:12 targetMeanPrice                45.88
POSH            21/08/20 17:20:12 targetHighPrice                54
POSH            21/08/20 17:20:12 totalCash                      579484992
POSH            21/08/20 17:20:12 totalDebt                      0
POSH            21/08/20 17:20:12 totalRevenue                   300812000
POSH            21/08/20 17:20:12 totalCashPerShare              7.605
POSH            21/08/20 17:20:12 financialCurrency              USD
POSH            21/08/20 17:20:12 revenuePerShare                7.34
POSH            21/08/20 17:20:12 quickRatio                     3.236
POSH            21/08/20 17:20:12 recommendationMean             2.1
POSH            21/08/20 17:20:12 exchange                       NMS
POSH            21/08/20 17:20:12 shortName                      Poshmark, Inc.
POSH            21/08/20 17:20:12 longName                       Poshmark, Inc.
POSH            21/08/20 17:20:12 exchangeTimezoneName           America/New_York
POSH            21/08/20 17:20:12 exchangeTimezoneShortName      EDT
POSH            21/08/20 17:20:12 isEsgPopulated                 False
POSH            21/08/20 17:20:12 gmtOffSetMilliseconds          -14400000
POSH            21/08/20 17:20:12 quoteType                      EQUITY
POSH            21/08/20 17:20:12 symbol                         POSH
POSH            21/08/20 17:20:12 messageBoardId                 finmb_144130423
POSH            21/08/20 17:20:12 market                         us_market
POSH            21/08/20 17:20:12 enterpriseToRevenue            4.733
POSH            21/08/20 17:20:12 enterpriseToEbitda             -197.554
POSH            21/08/20 17:20:12 52WeekChange                   -0.74591136
POSH            21/08/20 17:20:12 forwardEps                     -0.13
POSH            21/08/20 17:20:12 sharesOutstanding              40286600
POSH            21/08/20 17:20:12 bookValue                      5.484
POSH            21/08/20 17:20:12 sharesShort                    3323660
POSH            21/08/20 17:20:12 sharesPercentSharesOut         0.043899998
POSH            21/08/20 17:20:12 lastFiscalYearEnd              1609372800
POSH            21/08/20 17:20:12 heldPercentInstitutions        0.30016002
POSH            21/08/20 17:20:12 netIncomeToCommon              -73458000
POSH            21/08/20 17:20:12 trailingEps                    -1.793
POSH            21/08/20 17:20:12 SandP52WeekChange              0.29690683
POSH            21/08/20 17:20:12 priceToBook                    4.8194747
POSH            21/08/20 17:20:12 heldPercentInsiders            0.00958
POSH            21/08/20 17:20:12 nextFiscalYearEnd              1672444800
POSH            21/08/20 17:20:12 mostRecentQuarter              1625011200
POSH            21/08/20 17:20:12 shortRatio                     3.3
POSH            21/08/20 17:20:12 sharesShortPreviousMonthDate   1625011200
POSH            21/08/20 17:20:12 floatShares                    9488987
POSH            21/08/20 17:20:12 enterpriseValue                1423771136
POSH            21/08/20 17:20:12 priceHint                      2
POSH            21/08/20 17:20:12 priceToSalesTrailing12Months   6.699404
POSH            21/08/20 17:20:12 dateShortInterest              1627603200
POSH            21/08/20 17:20:12 forwardPE                      -203.30771
POSH            21/08/20 17:20:12 shortPercentOfFloat            0.2156
POSH            21/08/20 17:20:12 sharesShortPriorMonth          4107429
POSH            21/08/20 17:20:12 impliedSharesOutstanding       76249000
POSH            21/08/20 17:20:12 previousClose                  25.79
POSH            21/08/20 17:20:12 regularMarketOpen              25.78
POSH            21/08/20 17:20:12 twoHundredDayAverage           45.632645
POSH            21/08/20 17:20:12 payoutRatio                    0
POSH            21/08/20 17:20:12 regularMarketDayHigh           26.575
POSH            21/08/20 17:20:12 averageDailyVolume10Day        2043250
POSH            21/08/20 17:20:12 regularMarketPreviousClose     25.79
POSH            21/08/20 17:20:12 fiftyDayAverage                36.894855
POSH            21/08/20 17:20:12 open                           25.78
POSH            21/08/20 17:20:12 averageVolume10days            2043250
POSH            21/08/20 17:20:12 regularMarketDayLow            25.72
POSH            21/08/20 17:20:12 currency                       USD
POSH            21/08/20 17:20:12 regularMarketVolume            133828
POSH            21/08/20 17:20:12 marketCap                      2015261056
POSH            21/08/20 17:20:12 averageVolume                  1011939
POSH            21/08/20 17:20:12 dayLow                         25.72
POSH            21/08/20 17:20:12 ask                            26.45
POSH            21/08/20 17:20:12 askSize                        4000
POSH            21/08/20 17:20:12 volume                         133828
POSH            21/08/20 17:20:12 fiftyTwoWeekHigh               104.98
POSH            21/08/20 17:20:12 fiftyTwoWeekLow                25.62
POSH            21/08/20 17:20:12 bid                            26.33
POSH            21/08/20 17:20:12 tradeable                      False
POSH            21/08/20 17:20:12 bidSize                        1000
POSH            21/08/20 17:20:12 dayHigh                        26.575
POSH            21/08/20 17:20:12 regularMarketPrice             26.43
POSH            21/08/20 17:20:12 logo_url                       https://logo.clearbit.com/poshmark.com
```
