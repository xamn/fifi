import os, argparse, random, datetime, re, yfinance, threading, pickle, requests, math, time

arg_parser = argparse.ArgumentParser(description="Finance Filter Stock Tool")
arg_parser.add_argument("--verbose",       action="store_true")
arg_parser.add_argument("--data-file",     type=str)
arg_parser.add_argument("--stocks",        type=str)
arg_parser.add_argument("--limit",         type=int)
arg_parser.add_argument("--no-new",        action="store_true")
arg_parser.add_argument("--latest",        action="store_true")
arg_parser.add_argument("--dload",         action="store_true")
arg_parser.add_argument("--date",          type=str)
arg_parser.add_argument("--filter",        type=str)
arg_parser.add_argument("--fields",        type=str)
arg_parser.add_argument("--sort",          type=str)
arg_parser.add_argument("--group",         action="store_true")
args = arg_parser.parse_args()

# load global data
if args.data_file is None:
  args.data_file = "fifi.data"

if not os.path.isfile(args.data_file):
  pickle.dump({}, open(args.data_file, "wb"))

glob_dat = pickle.load(open(args.data_file, "rb"))

# stocks
if args.stocks is None:
  stocks = glob_dat.keys()
else:
  stocks = []
  
  for stock in args.stocks.split(","):

    # stocks from exchange
    if stock.startswith("exchange:"):
      splits = stock[len("exchange:"):].split("-")
      if len(splits) == 2:
        [se, mic] = splits
        if args.verbose: print("fetching stocks for exchange", se, "with mic", mic, "from FINNHUB...")
        req = requests.get("https://finnhub.io/api/v1/stock/symbol?exchange=" + se + "&mic=" + mic + "&token=c48h2riad3ief8nk366g").json()
      else:
        [se] = splits
        if args.verbose: print("fetching stocks for exchange", se, "from FINNHUB...")
        req = requests.get("https://finnhub.io/api/v1/stock/symbol?exchange=" + se + "&token=c48h2riad3ief8nk366g").json()
      stocks = [stock["symbol"] for stock in filter(lambda a: a["type"] == "Common Stock", req)]
      if args.verbose: print("done!")

    # manual stocks
    else:
      stocks.append(stock.upper())
  
  # remove new stocks if specified
  if args.no_new:
    stocks = [stock for stock in stocks if stock in glob_dat]

  if args.limit is not None:
    stocks = stocks[:args.limit]
  
  # remove spaces
  stocks = [stock.replace(" ", "-") for stock in stocks]

# dates
dates = None
if args.date is not None:
  dates = []

  for date in args.date.split(","):
    if date == "today":
      dates.append((datetime.datetime.today(), "day"))
    else:
      for format, _range in [("%y", "year"), ("%y/%m", "month"), ("%y/%m/%d", "day")]:
        try:
          dates.append((datetime.datetime.strptime(date, format), _range))
          break
        except:
          pass

# filters
filters = None
ident_re = re.compile("%([A-Za-z]+)")
if args.filter is not None:
  filters = []

  def make_filter(filter_str):
    filter_str = ident_re.sub(lambda r: "fields[\"" + r.group(1) + "\"]", filter_str)
    return eval("lambda fields:" + filter_str)

  filters.append(make_filter(args.filter))

def _filter(data):
  if filters is None:
    return True
  for f in filters:
    try:
      if not f(data):
        return False
    except:
      return False
  return True

fields = None
if args.fields is not None:
  fields = []

  for field in args.fields.split(","):
    if field.strip() != "":
      fields.append(field)

def dload(to_dload):
  if args.verbose: print("downloading data...")
  tickers = list(yfinance.Tickers(to_dload).tickers.items())
  def thread_fn():
    while len(tickers) != 0:
      key, value = tickers.pop()
      if args.verbose: print("downloading", key)
      while True:
        try:
          info = value.info
          if "longName" in info:
            if args.verbose: print("downloaded", key.ljust(10))
            if key not in glob_dat: glob_dat[key] = {}
            glob_dat[key][datetime.datetime.now()] = info
          else:
            stocks.remove(key)
            print("stock data not found for", key)
          break
        except:
          print("retrying", key, "in", "5s")
          time.sleep(5)


  threads = []
  for thread in range(500):
    thread = threading.Thread(target=thread_fn)
    threads.append(thread)
    thread.start()
  for thread in threads:
    thread.join()

def in_date(date, stock_date):
  dt, scope = date
  if scope == "year":
    return stock_date.year == dt.year
  if scope == "month":
    return stock_date.year == dt.year and stock_date.month == dt.month
  if scope == "day":
    return stock_date.year == dt.year and stock_date.month == dt.month and stock_date.day == dt.day

# determine stocks to download
to_dl = set()
if args.dload:
  to_dl.update(stocks)
else:
  for stock_name in stocks:
    if stock_name not in glob_dat:
      if args.latest:
        to_dl.add(stock_name)
      elif dates is not None:
        for date in dates:
          if in_date(date, datetime.datetime.now()):
            to_dl.add(stock_name)
            break
    elif dates is not None:
      for date in dates:
        if in_date(date, datetime.datetime.now()):
          for stock_date, stock_data in glob_dat[stock_name].items():
            if in_date(date, stock_date):
              break
            else:
              to_dl.add(stock_name)
if len(to_dl) != 0:
  dload(list(to_dl))

# local data
loc_dat = {}
for stock_name in stocks:
  loc_dat[stock_name] = {}
  if stock_name in glob_dat:
    for stock_date, stock_data in glob_dat[stock_name].items():
      loc_dat[stock_name][stock_date] = {}
      if dates is None:
        if not args.latest:
          if _filter(stock_data):
            for key, value in stock_data.items():
              if fields is None or key in fields:
                loc_dat[stock_name][stock_date][key] = value
      else:
        for date in dates:
          if in_date(date, stock_date):
            if _filter(stock_data):
              for key, value in stock_data.items():
                if fields is None or key in fields:
                  loc_dat[stock_name][stock_date][key] = value
            break
if args.latest:
  for stock_name, stock_data in glob_dat.items():
    if stock_name in stocks:
      stock_dates = stock_data.keys()
      latest_date = max(stock_dates)
      loc_dat[stock_name][latest_date] = {}
      if _filter(stock_data[latest_date]):
        for key, value in stock_data[latest_date].items():
          if fields is None or key in fields:
            loc_dat[stock_name][latest_date][key] = value

# sort
if args.sort is not None:
  def sort_fn(a):
    try:
      b = None
      stock_name, stock_data = a
      for stock_date, stock_data in stock_data.items():
        if args.sort in stock_data:
          if b is None:
            stock_data[args.sort]
          else:
            b = max(b, stock_data[args.sort])
      return b
    except:
      return -1
  loc_dat = dict(sorted(loc_dat.items(), key=sort_fn))

# print
if args.group:
  vals = set()
else:
  print("STOCK           DATE              FIELD                          VALUE")
for stock_name, stock_data in loc_dat.items():
  if stock_data != {}:
    for stock_date, stock_data in stock_data.items():
      for key, value in stock_data.items():
        if value is not None and value != "":
          if args.group:
            try: vals.add(value)
            except: pass
          else:
            print(stock_name.ljust(15), stock_date.strftime("%y/%m/%d %H:%M:%S"), str(key).ljust(30), value)
if args.group:
  for val in vals:
    print(val)

pickle.dump(glob_dat, open(args.data_file, "wb"))